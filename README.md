MarkovChainRandomWords
======================

A class to generate Markov-chain random words from a given list of words.




## How it works:    

We receive a list of words that seed our algorithm. The algorithm then populates a    
dictionary with prefixes as keys and a list of possible suffixes as values.

The idea is that when we create a random word we can only use combinations of     
letters that have already been observed in the seeding list of words.     
This creates random, yet 'pronounceable' words.    

If for example our input is one word: "science" and we have a chainlength of 2,    
the populated dictionary would be:    

```  
dict = {    
    '  ' : ['s'], #We insert two spaces before the word, so '  ' is one of the prefixes    
    ' s' : ['c'],     
    'sc' : ['i'],   
    'ci' : ['e'],     
    'ie' : ['n'],     
    'en' : ['c'],     
    'nc' : ['e'],     
    'ce' : ['\n'] #We add a newline character as a suffix for the end of the word.     
}  
```   
     
Now when we create a word, we start with two spaces: '  '    
That's our first prefix, and it can only be followed by 's' as 's' is the only available suffix for '  '.    
Now our word is ' s' and because of our very limited seed, the only available suffix for ' s' is 'c'.    
    
Obviously, the bigger the seeding list of words, the greater variety we can have !    

Notice that the size of the chainlength is important, bigger chainlengths produce     
words more similar to the original words while shorter chainlengths produce more     
random words. If chainlength is set to 1 you will encounter  words with triple (or more)      
consonants since the 'memory' of the algorithm is only one letter deep :      
e.g. if the word "suggest" is in the seeding list of words and chainlength is 1,     
the algorithm knows that 'g' can be followed by 'g', the new 'g' can also be followed       
by 'g' and so on, but if chainlength is 2, 'ug' ('u' chosen randomly) can be followed      
by another 'g' but then 'gg' cannot be followed by a third one except if you have a       
word with 'ggg' in your seeding list.      
